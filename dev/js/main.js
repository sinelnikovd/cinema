$(document).ready(function() {
  $('.last-comment .frame').mCustomScrollbar({
    axis: "y",
    scrollbarPosition: "outside",
    scrollInertia: 300,
    autoDraggerLength: false
  });

  $('.soon-films .slider').slick({
    arrows: true,
    prevArrow: '<span class="slick-prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slick-next"><i class="fa fa-angle-right"></i></span>',
    autoplay: true

  });

  function changeOwlPreview () {
    var preview = $('.top-carousel__preview');
    var previewLeft = $('.owl-item.active').offset().left - $('.owl-item.active .top-carousel__preview').offset().left;
    preview.css({
      'transform' : 'translateX(-'+ (Math.abs(previewLeft) + 50) +'px)'
    })
  }

  $('.top-carousel').on('initialized.owl.carousel',function() {
    changeOwlPreview();
  })
  .owlCarousel({
    loop:true,
    items: 1,
    //autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 800,
    nav: true,
    navText: ['<span class="owl-c-text">назад</span> <span class="owl-c-icon"><i class="fa fa-angle-left"></i></span>',
      '<span class="owl-c-icon"><i class="fa fa-angle-right"></i></span> <span class="owl-c-text">вперед</span>'],
    smartSpeed: 800,
  }).on('resized.owl.carousel', function(event) {
    changeOwlPreview();
  });

  $('.header__category').click(function() {
    $('.header').toggleClass('category-active');
  });

  $('.search__toggle').click(function() {
    if($('.search').hasClass('is-active')){
      $('.search').removeClass('is-active');
    }else{
      $('.search').addClass('is-active');
      $('.search input').focus();
    }
  });

  $('.search input').blur(function() {
    if($(this).val().length <= 0){
      $('.search').removeClass('is-active');
    }
  });

  function fixedHeader () {
    var scrollTop = $(window).scrollTop();
    var startPosition = ($(".top-carousel").length > 0) ? $(".top-carousel").offset().top : 0;
    var startShowPosition = ($('.category_block').length > 0) ? $('.category_block').offset().top + $('.category_block').outerHeight() : 0;
    if(scrollTop > startPosition){
      $('body').addClass('is-fixed-header');
    }else{
      $('body').removeClass('is-fixed-header');
    }

    if(scrollTop > startShowPosition){
      $('body').addClass('is-fixed-header-show');
    }else{
      $('body').removeClass('is-fixed-header-show');
    }

  }

  fixedHeader();


  $(window).scroll(function() {
    fixedHeader();
    $('.header').removeClass('category-active');
  });



  $('.header .category').mCustomScrollbar({
    axis: "y",
    scrollbarPosition: "inside",
    scrollInertia: 300,
    autoDraggerLength: false
  });


  plyr.setup();
  

  $('.film-tab__navs a').click(function() {
    if($(this).hasClass('is-active')) return false;

    $('.film-tab__navs li.is-active').removeClass('is-active');
    $(this).closest('li').addClass('is-active');

    var players = plyr.get();
    for(var i = 0; i< players.length; i++){
      if($(players[i].getContainer()).closest('.film-tab__tab-content.is-active').length > 0){
        players[i].pause();
      }
    }
    $('.film-tab__tab-content.is-active').removeClass('is-active');
    $($(this).attr('href')).addClass('is-active');

    return false;
  });

  $(".fancybox").fancybox({
    loop: true,
    buttons : [
        'slideShow',
        'fullScreen',
        'close'
    ],
  });

  $('.catalog__view-line, .catalog__view-grid').click(function() {
    if($(this).hasClass('is-active')) return false;

    if($(this).hasClass('catalog__view-line')){
      $('.catalog.is-grid').removeClass('is-grid');
    }
    if($(this).hasClass('catalog__view-grid')){
      $('.catalog').addClass('is-grid');
    }
    $('.catalog__view-line.is-active, .catalog__view-grid.is-active').removeClass('is-active');
    $(this).addClass('is-active');
    return false;
  });

});